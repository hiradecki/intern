#サーバー上にDBを作成するときだけ行う操作
#ちゃんとDBが作成されたかは、ターミナルで確認
# ① sqlite3 2ndApp.db ← ターミナルで(Pythonを経由せず)DBに接続
# ② select * from Items; などSQL文入力
# で確認可能

import sqlite3

# DB作成
dbname = 'App.db'
conn = sqlite3.connect(dbname) #2ndApp.dbという名のDBに接続
c = conn.cursor()
# Create文が長いので、事前に変数にしておく
employee_create = 'CREATE TABLE IF NOT EXISTS Employees(emplooyee_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,employee_name TEXT NOT NULL,password INTEGER NOT NULL)'
event_create = 'CREATE TABLE Events (event_id INTEGER NOT NULL,event_name TEXT NOT NULL,reserve_seat INTEGER NOT NULL,event_date TEXT NOT NULL,event_time TEXT NOT NULL, employee_id INTEGER, PRIMARY KEY(event_id AUTOINCREMENT))'
orderItems_create = 'CREATE TABLE OrderedItems (reservation_id	INTEGER NOT NULL, item_id INTEGER NOT NULL,item_pieces INTEGER NOT NULL,FOREIGN KEY(item_id) REFERENCES Items(item_id),FOREIGN KEY(reservation_id) REFERENCES Reservations(reservation_id))'
reservations_create = 'CREATE TABLE Reservations (reservation_id INTEGER NOT NULL, reservation_date	TEXT NOT NULL, reservation_time	TEXT NOT NULL, cupon INTEGER NOT NULL DEFAULT 0, sum INTEGER NOT NULL DEFAULT 0,here_or_togo INTEGER NOT NULL DEFAULT 0, finish TEXT NOT NULL DEFAULT 0, employee_id INTEGER, FOREIGN KEY(employee_id) REFERENCES Employees(emplooyee_id), PRIMARY KEY(reservation_id AUTOINCREMENT))'
seats_create = 'CREATE TABLE Seats (date TEXT NOT NULL, time TEXT NOT NULL, seat_num INTEGER NOT NULL DEFAULT 30, last_seat	INTEGER DEFAULT 30, PRIMARY KEY(date,time))'
items_create = 'CREATE TABLE Items (item_id	TEXT NOT NULL, item_name TEXT NOT NULL, price INTEGER NOT NULL, category TEXT NOT NULL, PRIMARY KEY(item_id))'

try:
    
    #作成したいテーブルがもしすでに存在したら、削除する。    
    c.execute("DROP TABLE IF EXISTS Employees")
    c.execute("DROP TABLE IF EXISTS Events")
    c.execute("DROP TABLE IF EXISTS OrderedItems")
    c.execute("DROP TABLE IF EXISTS Reservations")
    c.execute("DROP TABLE IF EXISTS Seats")
    c.execute("DROP TABLE IF EXISTS Items")
    
    # テーブル作成 (Create)
    c.execute(employee_create)
    c.execute(event_create)
    c.execute(orderItems_create)
    c.execute(reservations_create)
    c.execute(seats_create)
    c.execute(items_create)

    #プログラムからメニューを追加
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('creamSoda', 'クリームソーダ', 400, 'seasonMenus')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('lemonade', 'レモネード', 400, 'seasonMenus')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('kakigouri', 'かき氷', 400, 'seasonMenus')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('hotTea', 'ティー', 400, 'seasonMenus')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('hotLatte', 'カフェラテ', 390, 'hotDrinks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('hotCoffee', 'ブレンドコーヒー', 300, 'hotDrinks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('iceCoffee', 'アイスコーヒー', 300, 'iceDrinks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('iceTea', 'アイスティー', 310, 'iceDrinks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('bubbleTea', 'タピオカミルクティー', 550, 'iceDrinks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('German_dock', 'ジャーマンドッグ', 220, 'snacks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('Vegetable_Sandwich', '野菜サンド', 220, 'snacks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('toast', 'トースト', 260, 'snacks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('Cheese_toast', 'チーズトースト', 190, 'snacks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('Soybean_Meat_Sandwich', 'サンドウィッチ', 350, 'snacks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('SandWitch', 'サンドウィッチ', 350, 'snacks')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('cookie', 'クッキー', 150, 'sweets')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('scone', 'スコーン', 200, 'sweets')")
    c.execute("INSERT INTO Items (item_id, item_name,price, category) VALUES('muffin', 'マフィン', 180, 'sweets')")
    
except sqlite3.Error as e:
    print('sqlite3.Error occurred', e.args[0]) #もしデータベースの操作でエラーが発生した場合、このprint文を表示する。

conn.commit()
conn.close()

