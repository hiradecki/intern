$(document).ready(function(){
	// 日付選択ボタンクリック時の処理
	$('#datepicker_button').click(function(){
		$('#datetimepicker').datetimepicker('show'); //Date Time Picker表示
	});


	// 日付テキストボックス・時刻テキストボックスへ設定
	$('#datetimepicker').datetimepicker({
		format:'Y/m/d H:i ',
		step:30,
		minDate:'-1970/01/01',
		maxDate:'+1970/01/07',
		minTime:'10:00',
		maxTime:'17:00',
		onChangeDateTime:function(dp,$input){
			//日付と時間を分けて表示。
			//後ほど、この値で予約可能かDB参照する
			var datetime = $input.val().split(' ');
			$('#txtDate').val(datetime[0]);
			$('#txtTime').val(datetime[1]);
		}
	});

});
