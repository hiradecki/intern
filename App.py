# このファイルを実行することで処理が開始

from bottle import Bottle, route, run, request, post, template, static_file
from bottle_flash2 import FlashPlugin
import sqlite3

'''CSSを使う時などの静的ファイルをbottleで扱うための設定
# 参考サイト : https://tmg0525.hatenadiary.jp/entry/2018/03/04/004706
'''

import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_DIR = os.path.join(BASE_DIR, 'static')

@route('/static/<filePath:path>')
def send_static(filePath):
    #静的ファイルを返す
    return static_file(filePath, root='./static')

#Flash Setup
app = Bottle()
COOKIE_SECRET = 'super_secret_string'
app.install(FlashPlugin(secret=COOKIE_SECRET))

##############  DBの設定  ###########################
dbname = 'hirade.db'
conn = sqlite3.connect(dbname)  #接続
c = conn.cursor()

##############  顧客側の画面  ###########################
#routeはURL
@route('/')
def welcome():
    return template('index', app = app)


#店内飲食の場合。 席予約を取る。
@route('/seatReservation')
def seat_reservation():
    # テンプレートを読み込み、データを適用してHTMLとしてレンダリング
    return template('seatReservation')

#テイクアウトの場合。受け取り予約を取る。
@route('/takeoutReservation')
def takeout_reservation():
    # テンプレートを読み込み、データを適用してHTMLとしてレンダリング
    return template('takeoutReservation')

@route('/checkAvalability',method='POST')
def check_availability():
    # POSTリクエストからdateとtimeを取得
    date = request.forms.get('date')
    time = request.forms.get('time')
    time_str = str(time) + ":00"
    here_or_togo = request.forms.getunicode('here_or_togo')
    # ここでdateとtimeを使ってDB処理を行う
    conn = sqlite3.connect(dbname)
    c = conn.cursor() #カーソルオブジェクトを作る
    
    query = "SELECT last_seat FROM Seats WHERE date = '"+ date + "' and time= +"' time_str + '" "
    last_seats = c.excute(query).fetchone()

    conn.close()
    last_seats=int(last_seats[0])

    # DB処理結果に応じて予約可能かどうかのデータを返す
    if last_seats > 0:
        # 予約可能な場合の画面
        content="""
        <p> 予約可能です。</p>
        <form action="/menu" value="この日時で予約する">
            <input type = "submit" value="この日時で予約する">
            <input type = "hidden" name = "date" value = '{{date}}'>
            <input type = "hidden" name = "time" value = '{{time}}'>
                                    <input type = "hidden" name ="here_or_togo" value='{{here_or_togo}}'>
        </form>
        """
        return template(content, date=date, time=time, here_or_togo=here_or_togo)
    else:
        # 満席などの場合の画面
        content = """
        <p> 予約できません。他の日時で再度お試し下さい。 </p>
        <input type="button" class="back" onclick="history.back()" value="戻る">
        """
        return template(content)

#メニュー一覧を表示、選択
@route('/menu',method='POST')
def menu():
    #POSTリクエストのパラメータ受け取り
    date = request.forms.getunicode('date')
    time = request.forms.getunicode('time')
    here_or_togo = request.forms.getunicode('here_or_togo')

    item_list = get_item()
    category_dict = {"seasonMenus":"期間限定", "hotDrinks":"ホット","iceDrinks":"アイス", "snacks":"軽食","sweets":"焼き菓子"}
    return template('menu',date=date, time=time, here_or_togo=here_or_togo,item_list=item_list,category_dict=category_dict)

#メニュー一覧をDBから取り出すメソッド
def get_item():
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    select = "select item_id, item_name, price, category from Items"
    c.execute(select)
    # selectした全メニューを、配列の辞書に受け取り(配列の一つの要素が、1商品の情報を表した表した辞書になる。)
    item_list=[]
    for row in c.fetchall(): #DBに入っている商品すべてに対して繰り返して処理する。
        item_list.append({#item_listに辞書型の1メニューを表す要素を追加
                        "item_id":row[0],
                        "item_name":row[1],
                        "price":row[2],
                        "category":row[3] 
                        } )
    conn.close()
    return item_list
    
#注文確認画面
@route('/confirm',method='POST')
def confirm():
    # テンプレートを読み込み、データを適用してHTMLとしてレンダリング
    return template('confirm')

'''
#エラーが出たため、以下のconfirm関連の設定をコメントアウト
@route('/confirm',method='POST')
def confirm():

    
    cupon = request.forms.getunicode('cupon')

    #DBの準備
    conn = sqlite3.connect(dbname)
    c = conn.cursor()

    #合計を求める変数(sum)を定義
    sum = 0

    #メニュー選択をdata_listへ受け取り
    data_list = []
    for key, value in request.forms.items(): #menu.html(注文画面)から値受け取り
        #keyはitem_id、valueは注文の個数。
        #以下の要素はdata_listに含めない。
        if key.startswith('tab'):
            continue # タブの選択状態
        if key.startswith('cupon'):
            continue #クーポン
        if key.startswith('date'):
            continue #予約日
        if key.startswith('time'):
            continue #予約時間
        if key.startswith('here_or_togo'):
            continue # 店内飲食かテイクアウトか

        if value != '0':
            select_name = "select item_name from Items where item_id ='" +key+"'"
            select_price = "select price from Items where item_id ='"+ key +"' "
            name = c.execute(select_name).fetchone() #('クリームソーダ')という状態
            #nameはtuple型なので、String型へ変更⇒スライスで要らない('と', )をなくした状態
            name_str = str(name)
            name = name_str[2:-3] #要らない部分が削除された商品名が格納
            result = c.execute(select_price).fetchone()
            if result:
                price = int(result[0])
            else:
                price = 0

            sum = sum + int(price) * int(value) #値段を入れる

            data_list.append({ #data_list配列に1注文分の辞書を追加
                "item_id":key,
                "item_name": name, 
                "price":price,
                "namber":int(value)
            })
    conn.close()

    # データの引き継ぎ
    date = request.forms.getunicode('date')
    time = request.forms.getunicode('time')
    here_or_togo = int(request.forms.getunicode('here_or_togo'))

    here_or_togo_text =""
    if int(here_or_togo) == 0:
        here_or_togo_text = "テイクアウト"
    elif int(here_or_togo) == 1:
        here_or_togo_text = "店内飲食"
    else:
        here_or_togo_text = "未選択"

    return template('confirm') #,data_list, cupon=cupon, date=date,time=time, here_or_togo_text=here_or_togo_text, here_or_togo=here_or_togo,sum=sum)
'''

    
if __name__ == "__main__":
    run(host='localhost', port=8000, reload=True, debug=True)